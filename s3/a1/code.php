<?php 

class Person {

	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName(){
		return "Your full name is $this->firstName $this->middleName $this->lastName";
	}

}



class Developer extends Person {

	public function printName(){
		return "Your name is  $this->firstName $this->middleName $this->lastName and you are a developer";
	}

}


class Engineer extends Person {

	public function printName(){
		return "Your are an engineer named  $this->firstName $this->middleName $this->lastName";
	}

}


$person = new Person('Van Excel', 'Busalla', 'Rondina');
$developer = new Developer('Jan Rhea', 'Opsima', 'Velasco');
$engineer = new Engineer('Ella', 'Rondina', 'Tampos');