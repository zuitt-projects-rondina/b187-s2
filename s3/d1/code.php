<?php 

//Objects as variables

$buildingObj = (object)[
	'name' => 'Caswyn Building',
	'floors' => 8,
	'address' => (object)[
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippies'

		]
	
];

class Building {
	public $name;
	public $floors;
	public $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	// methods
	public function printName(){
		return "The name of the building is $this->name";
	}

}

$building = new Building('Caswyn Building', 8, 'Timog Avenue, Quezon City, Philippines');

$building2 = new Building('Manulife Building', 51, 'Commonwealth Avenue, Quezon City, Philippines');