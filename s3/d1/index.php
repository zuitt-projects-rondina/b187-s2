<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
	<h1>Objects from Variables</h1>
	<p><?php var_dump($buildingObj); ?></p>

	<h1>Objects from Class</h1>
	<p><?php var_dump($building); ?></p>
	<p><?php var_dump($building2); ?></p>
	<p><?= $building->printName(); ?></p>
</body>
</html>