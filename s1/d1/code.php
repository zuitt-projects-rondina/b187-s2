<?php

//echo is used to output data to the screen
/*echo "Mabuhay World";*/

$name = "John Smith";
$email = "johnsmith@mail.com";



define('PI', 3.1416);

// DATA TYPES

$state = "New York";
$country = "USA";
$address = $state . ',' . $country; // concatenate via dot.
$address2 = "$state, $country"; // concatenate via double quotes

//Integers
$age = 31;
$headcount = 26;

//Floats
$grade = 98.2;
$distance = 1342.12;

//Boolean
$hastravelledAbroad = true;

//Null
$spouse = null;

//Arrays
$grades = array(98.7, 92.1, 90.2, 94.6);

//Objects
$gradesObj = (object) [
	'firstGrading' => 87.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 92.1,
	'fourthGrading' => 92.1

];

$personObj = (object) [
	'fullName' => "John Smith",
	'isMarried' => false,
	'age' => 34,
	'address' => (object)[
		'state' => "New York",
		'country' => "USA"
	]
];



//OPERATORS

//Assignment Operator (=)

$x = 234;
$y = 123;

$isLegalAge = true;
$isRegistered = false;



// FUNCTIONS

function getFullName($firstName, $middleInnitial, $lastName){
	return "$lastName, $firstName, $middleInnitial";
}


// SELECTION CONTROL STRUCTURES

function determineTyphoonIntensity($windSpeed){
	if($windSpeed < 30){
		return "Not a typhoon yet";

	}else if($windSpeed <= 30){
		return "Tropical depression detected";

	}else if($windSpeed >= 62 && $windSpeed <= 88){
		return "Tropical storm detected";

	}else if($windSpeed >= 89 && $windSpeed <= 117){
		return "Severe tropical storm detected";

	}else {
		return "Typhoon detected";
	}
}


//Ternary Operator

function isUnderAge($age){
	return ($age < 18) ? true : false;
}

//Switch Statement
function determineUser($computerNumber){
	switch($computerNumber){
		case 1:
			return 'Linus Torvalds';
			break;
		case 2:
			return 'Steve Jobs';
			break;
		case 3:
			return 'Sid Meir';
			break;
		default:
			return "The computer number $computerNumber is out of bounds";
			break;

	};
}