<!--  -->
<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S01</title>
</head>
<body>
	<h1>Variables:</h1>
	<p><?php echo "Good day $name! Your email is $email"; ?></p>
	<!-- When echoing constant, omit $ sign -->
	<p><?php echo PI; ?></p>
	<p><?php echo $address; ?></p>
	<p><?php echo $address2; ?></p>

	<h1>DATA TYPES</h1>
	<p><?php echo $hastravelledAbroad; ?></p>
	<p><?php echo $spouse; ?></p>

	<p><?php echo gettype($spouse); ?></p>
	<p><?php  var_dump($hastravelledAbroad); ?></p>
	<p><?php  print_r($grades); ?></p>
	<p><?php echo $gradesObj->firstGrading; ?></p>
	<p><?php echo $personObj->address->state; ?></p>

	<h1>Operators</h1>
	<p>Sum: <?php echo $x + $y; ?></p>
	<p>Difference: <?php echo $x - $y; ?></p>
	<p>Product: <?php echo $x * $y; ?></p>
	<p>Qoutient: <?php echo $x / $y; ?></p>

	<h1>EQUALITY OPERATORS</h1>
	<p>Loose Equality : <?php echo var_dump($x === 234); ?></p>
	<p>Loose Inequality : <?php echo var_dump($x != 123); ?></p>
	<p>Strict Equality : <?php echo var_dump($x === 123); ?></p>
	<p>Strict Inequality : <?php echo var_dump($x !== 123); ?></p>

	<h3>Greater/Lesser Operators</h3>
	<p>Is Lesser (or equal): <?php echo var_dump($x < $y); ?></p>
	<p>Is Greater (or equal): <?php echo var_dump($x >= $y); ?></p>

	<h3>Logical Operators</h3>
	<p>Are All Requirements Met: <?php var_dump($isLegalAge && $isRegistered); ?></p>
	<p>Are Some Requirements Met: <?php var_dump($isLegalAge || $isRegistered); ?></p>
	<p>Are Some Requirements Not Met: <?php var_dump(!$isLegalAge && !$isRegistered); ?></p>


	<h1>Full Name: <?php echo getFullName("John", "J", "Smith"); ?></h1>
	<p><?php echo determineTyphoonIntensity(13); ?></p>

	<h2>Ternary Operator</h2>
	<p><?php var_dump(isUnderAge(78)); ?></p>
	<p><?php var_dump(isUnderAge(15)); ?></p>

	<h3>Switch Statement</h3>
	<p><?php echo determineUser(2); ?></p>



</body>
</html>