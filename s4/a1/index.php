<?php require_once("./code.php"); ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S4</title>
</head>
<body>

	<h1>Building</h1>
	<?= $building->printProperties(); ?>
	<?php $building->setName('Caswynn Complex'); ?>
	<?php echo $building->getName(); ?>



	<h1>Condo</h1>
	<?= $condominium->printProperties(); ?>
	<?php $condominium->setCondoName('Enzo Tower'); ?>
	<?php echo $condominium->getCondoName(); ?>

</body>
</html>
