<?php

class Building {
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	public function printProperties(){
		return "The name of the building is $this->name </br>
				The $this->name has $this->floors floors </br>
				The $this->name is located at  $this->address </br>";
	}



	public function getName(){
		return "The name of the building has been changed to $this->name" . "</br>";
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function setFloors($floors){
		$this->floors = $floors;
	}

	public function getAddress(){
		return $this->address;
	 }

	public function setAddress($address){
		$this->address = $address;
	}
}

class Condominium extends Building {

	public function getCondoName(){
		return "The name of the condominium	 has been changed to $this->name" . "</br>";
	}

	public function setCondoName($name){
		$this->name = $name;
	}

	

}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');

