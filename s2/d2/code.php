<?php

/*REPETITION CONTROL STRUCTURES*/

function whileLoop(){
	$count = 0;

	while($count <= 5){
		echo $count . '</br>';
		$count++;
	}
}

// DO-WHILE LOOP

function doWhileLoop(){
	$count = 20;

	do{
		echo $count . '</br>';
		$count--;
	}while($count > 0);
}


//FOR-LOOP

function forLoop(){
	for($i = 0; $i <= 10; $i++){
		echo $i . '</br>';
	}
}

// Array Manipulation

$studentNumbers = array('1923', '1924', '1925', '1926');

$grades = [98.5, 94.3, 89.2, 90.1];

// Associative Arrays

$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];

//Multi-Dimensional Arrays
$heroes = [
	['Iron Man', 'Thor', 'Hulk'],
	['Wolverine', 'Cyclops', 'Jean Grey'],
	['Batman', 'Superman', 'Wonder Woman']
];
	

//Multi-Dimensional Associative Arrays

$powers = [
	'regular' => ['Repulsor Blast', 'Rocket Punch'],
	'signature' => ['Unibeam']
];

$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

//Searching arrays:
function searchBrand($brand, $brandsArr){
	if(in_array($brand, $brandsArr)){
		return "$brand is in the array.";
	}else{
		return "$brand is NOT in the array.";
	}
}

