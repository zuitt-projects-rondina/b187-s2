<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S02</title>
</head>
<body>
	<h1>Repetition Control Structures</h1>
	<h3>While loop</h3>
	<?php whileLoop(); ?>

	<h3>Do-While loop</h3>
	<?php doWhileLoop(); ?>

	<h3>For loop</h3>
	<?php forLoop(); ?>

	<h1>Array Manipulation</h1>

	<h2>Types of Array</h2>

	<h2>Simple Array</h2>
	<ul>
			<?php foreach($grades as $grade){ ?>
				<li><?php echo $grade; ?></li>
			<?php } ?>


	</ul>
	<h2>Associative Array</h2>


	<ul>
		<?php foreach($gradePeriods as $period => $grade){ ?>
			<li>Grade in <?= $period; //<?= is the shortcut for <?php echo ?> is <?= $grade; ?></li>
		<?php } ?>		
	</ul>



	<h3>Multi-Dimensional Array</h3>
	<ul>
		<?php
			foreach($heroes as $team){
				foreach($team as $member){
					?>
						<li><?= $member; ?></li>
					<?php }
			}
		?>
	</ul>
		<?php
			for($i = 0; $i < count($heroes); $i++){
				for($j = 0; $j < count($heroes[$i]); $j++){
					echo $heroes[$i][$j]; ?> </br>
					<?php
				}
			}
		?>

	<h3>Multi-Dimensional Associative Array</h3>
		<?php
			foreach($powers as $label => $powerGroup){
				foreach($powerGroup as $power){
					?>
						<li><?= "$label: $power"; ?></li>
					<?php }
				}
		?>


	<h1>Array Functions</h1>

	<h3>Add to Array</h3>

	<?php array_push($computerBrands, 'Apple'); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<?php array_unshift($computerBrands, 'Dell'); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Remove from Array</h3>

	<?php array_pop($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<?php array_shift($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Sort/Reverse</h3>

	<?php sort($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<?php rsort($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Count</h3>
	<p><?= count($computerBrands); ?></p>

	<h3>In Array</h3>
	<p><?= searchBrand('HP', $computerBrands); ?></p>




</body>
</html>