<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
<h1>Divisible by 5</h1>
<?php divisibleByFive(); ?>

<h1>ARRAY MANIPULATION</h1>
<h2>Add a student and print</h2>

<?php array_push($students, 'Jan'); ?>
<p><?php print_r($students); ?></p>

<h2>Count</h2>
<p><?= count($students); ?></p>

<h2>Add another student and print the array and new count</h2>

<?php array_push($students, 'Van'); ?>
<p><?php print_r($students); ?></p>
<p><?= count($students); ?></p>


<h2>Remove the first student and print the array and its count</h2>
<?php array_shift($students); ?>
<p><?php print_r($students); ?></p>
<p><?= count($students); ?></p>
</body>
</html>
